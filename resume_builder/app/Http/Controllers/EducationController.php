<?php

namespace App\Http\Controllers;

use App\Education;
use Illuminate\Http\Request;


class EducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $education = auth()->user()->education->All();

        return view('education.index', ['education' => $education]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('education.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'school_name' => 'required',
            'school_location' => 'required',
            'degree' => 'required',
            'graduation_start_date' => 'required',
            'graduation_end_date' => 'required'
        ]);

        $detail = new Education();

        auth()->user()->education()->create($request->all());

        // $detail->school_name = $request->input('school_name');
        // $detail->school_location = $request->input('school_location');
        // $detail->degree = $request->input('degree');
        // $detail->graduation_start_date = $request->input('graduation_start_date');
        // $detail->graduation_end_date = $request->input('graduation_end_date');
        // $detail->user_id = auth()->id();

        // $detail->save();

        return redirect(url('/education'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function show(Education $education)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function edit(Education $education)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Education $education)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function destroy(Education $education)
    {
        //
    }
}
