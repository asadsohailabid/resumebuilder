@extends('layouts.app')

@section('content')
    <div class="container flex-container" style="display:flex;justify-content:center;align-items:center;height:80vh;">
        <div class="card" style="margin-left:auto;margin-right:auto;width:70%">
            <div class="card-header bg-white border-danger text-center">
                <h3 class="font-weight-bold">Education Details</h3>
            </div>
            <div class="card-body flex-container" style="display:flex;justify-content:center;align-items:center;">
                <div>
                    @if (session()->has('errors'))
                        @foreach (session('errors') as $error)
                            <div class="alert alert-danger">
                                <p>{{ $error }}</p>
                            </div>
                        @endforeach
                    @endif
                </div>
                <form class="form-horizontal" role="form" action="{{ url('/education') }}" method="POST">
                    @csrf
                    <div class="form-group row">
                        <label for="school_name">School Name:</label>
                        <input class="form-control" placeholder="Enter School Name" id="school_name" name="school_name" required />
                    </div>
                    <div class="form-group row">
                        <label for="school_location">School Location:</label>
                        <input class="form-control" placeholder="Enter School Location" id="school_location" name="school_location" required />
                    </div>
                    <div class="form-group row">
                        <label for="degree">Degree:</label>
                        <input class="form-control" placeholder="Enter Degree" id="degree" name="degree" required />
                    </div>
                    <div class="form-group row">
                        <label for="field_of_study">Field Of Study:</label>
                        <input class="form-control" placeholder="Enter Field Of Study" id="field_of_study" name="field_of_study" />
                    </div>
                    <div class="form-group row">
                        <label for="graduation_start_date">Start Date:</label>
                        <input class="form-control" placeholder="Enter Start Date" id="graduation_start_date" name="graduation_start_date" type="date"/>
                    </div>
                    <div class="form-group row">
                        <label for="graduation_end_date">End Date:</label>
                        <input class="form-control" placeholder="Enter End Date" id="graduation_end_date" name="graduation_end_date" type="date"/>
                    </div>
                    <div class="form-group row">
                        <label for="submit"></label>
                        <button type="submit" class="btn btn-danger btn-block" id="submit" name="submit">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
