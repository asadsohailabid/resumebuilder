@extends('layouts.app')

@section('content')
    <div class="container flex-container" style="display:flex;justify-content:center;height:80vh;">
        <div class="card" style="margin-left:auto;margin-right:auto;width:100%;">
            <div class="card-header bg-white border-danger text-center">
                <h1>EDUCATION SUMMARY</h1>
            </div>

            <div class="card-body" style="display:flex;justify-content:center;">
                @foreach ($education as $e)
                    <div class="card" style="width:90%;">
                        <div class="card-body row">
                            <div class="col-md-9 text-left" style="border-right:1px solid red">
                                <h3>{{ $e->degree }} {{ $e->school_name }}</h3>
                                from {{ explode('-', ((string) $e->graduation_start_date))[0] }} to
                                {{ explode('-', ((string) $e->graduation_end_date))[0] }}
                            </div>
                            <div class="col-md-2">
                                <button class="btn border-dark btn-sm btn-block">EDIT</button>
                                <button class="btn btn-danger btn-sm btn-block">DELETE</button>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection
