@extends('layouts.app')

@section('content')
    <div class="flex-container text-center row" style="display:flex; align-items:center; height:80vh;">
        <div class="col-md-7 text-right" style="border-right: 2px solid red">
            <label class="display-4">IMPRESSIVE RESUMES</label>
            <h2>EASILY BUILD YOUR RESUME</h2>
        </div>
        <div class="col-md-5 text-left">
            <a href="{{ url('/user-detail/create') }}" class="btn btn-danger btn-lg">Make My Resume!</a>
        </div>
    </div>
@endsection
