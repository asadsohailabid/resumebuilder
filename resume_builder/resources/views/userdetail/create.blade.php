@extends('layouts.app')

@section('content')
    <div class="container flex-container" style="display:flex;justify-content:center;align-items:center;height:80vh;">
        <div class="card" style="margin-left:auto;margin-right:auto;">
            <div class="card-header bg-white border-danger text-center">
                <h3 class="font-weight-bold">Personal Details About You</h3>
            </div>
            <div class="card-body flex-container" style="display:flex;justify-content:center;align-items:center">
                <div>
                    @if (session()->has('errors'))
                        @foreach (session('errors') as $error)
                            <div class="alert alert-danger">
                                <p>{{ $error }}</p>
                            </div>
                        @endforeach
                    @endif
                </div>
                <form class="form-horizontal" role="form" action="{{ url('user-detail/') }}" method="POST">
                    @csrf
                    <div class="form-group row">
                        <label for="fullname">Full Name:</label>
                        <input class="form-control" placeholder="Enter Full Name" id="fullname" name="fullname" required />
                    </div>
                    <div class="form-group row">
                        <label for="email">Email:</label>
                        <input class="form-control" placeholder="Enter Email" id="email" name="email" required />
                    </div>
                    <div class="form-group row">
                        <label for="phone">Phone:</label>
                        <input class="form-control" placeholder="Enter Phone" id="phone" name="phone" required />
                    </div>
                    <div class="form-group row">
                        <label for="address">Address:</label>
                        <input class="form-control" placeholder="Enter Address" id="address" name="address" />
                    </div>
                    <div class="form-group row">
                        <label for="submit"></label>
                        <button type="submit" class="btn btn-danger btn-block" id="submit" name="submit">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
